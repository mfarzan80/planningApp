package com.example.myclock.Database;

import com.example.myclock.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import TestSheet.TestSheet;

public class AllTestSheets {
    private static HashMap<Integer, TestSheet> TestSheetsHashMap = new HashMap<>();
    private static boolean hasBeenLoaded = false;
    static {
        load();
    }

    public static int AddToList(TestSheet p) {
        if (!hasBeenLoaded)
            load();

        int ID = MaxID.testSheetMaxID();
        p.setSelf_ID(ID);
        TestSheetsHashMap.put(ID, p);
       // MainActivity.databaseAdapter.addTestSheet(ID,p);

        return ID;
    }

    public static TestSheet getByID (int ID) {
        if (!hasBeenLoaded)
            load();

        return TestSheetsHashMap.get(ID);
    }

    public static void updateByID (Integer ID, TestSheet newTestSheet) {
        if (!hasBeenLoaded)
            load();

        if (ID == -1)
            return;

        TestSheetsHashMap.put(ID, newTestSheet);
      //  MainActivity.databaseAdapter.updateTestSheet(ID,newTestSheet);
    }


    public static int removeByTestSheet(TestSheet l) {
        if (!hasBeenLoaded)
            load();

        int ID = l.getSelf_ID();
        removeByID(ID);

        return ID;
    }


    public static ArrayList<TestSheet> getTestSheetsByListOfIDs (ArrayList<Integer> IDs) {
        if (!hasBeenLoaded)
            load();

        ArrayList<TestSheet> temp = new ArrayList<>();
        for (Integer ID : IDs) {
            TestSheet found = getByID(ID);
            if (found != null) {
                temp.add(found);
            }
        }
        return temp;
    }

    public static void removeByID (int ID) {
        if (!hasBeenLoaded)
            load();

        TestSheetsHashMap.remove(ID);
      //  MainActivity.databaseAdapter.removeTestSheet(ID);
    }

    public static void load () {
       // TestSheetsHashMap = MainActivity.databaseAdapter.getTestSheets();
        hasBeenLoaded = true;
    }
}
