package com.example.myclock.Database;

import java.util.ArrayList;

public class DailyInformation {
    private ArrayList<Integer> plansID = new ArrayList<>();

    public void addToDailyPlan (Plan plan) {
        plansID.add(plan.getSelf_ID());
    }

    public void removePlan (Integer ID) {
        plansID.remove(ID);
    }

//    public void removeWithRepeating (Plan p) {
//        plans.remove(p);
//        PropertyHolder.removeRepeatingPlan(p);
//    }

}
