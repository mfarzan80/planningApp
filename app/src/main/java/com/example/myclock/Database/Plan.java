package com.example.myclock.Database;
import android.util.Log;

import com.example.myclock.Views.CheckListContainerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: add period and alarm

public class Plan {
    private Integer self_ID;
    private Integer courseID;
    private int duration;
    private int studyTime;
    private int testTime;
    private HashMap<String, Boolean> checklists = new HashMap<>();
    private boolean notification;
    private int notificationInMinutes;



    public Plan(Course course, int duration, ArrayList<Long> repeatingDays,
                ArrayList<String> checkList, int notificationInMinutes) {

        this.courseID = course.getSelf_ID();
        this.duration = duration;
        this.notificationInMinutes = notificationInMinutes;

        for (Long day : repeatingDays) {
            DailyInformationHandler.addPlan(day, this);
        }

        for (String a : checkList)
            this.checklists.put(a, false);
    }

    public Plan(Course course, int duration, ArrayList<Long> repeatingDays,
                ArrayList<String> checkList) {

        this(course, duration, repeatingDays, checkList, -1);
    }

    public void setSelf_ID(Integer self_ID) {
        this.self_ID = self_ID;
    }

    public Integer getSelf_ID() {
        return self_ID;
    }

    public boolean hasNotification () {
        return notification;
    }

    public Course getCourse() {
        return AllCourses.getByID(courseID);
    }

    public void setCourse(Course course) {
        this.courseID = course.getSelf_ID();
        updateSql();
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int totalTime) {
        this.duration = totalTime;
        updateSql();
    }

    public int getPassedTime() {
        return studyTime + testTime;
    }

    public int getStudyTime() {
        return studyTime;
    }

    public int getTestTime() {
        return testTime;
    }

    public void setStudyTime(int studyTime) {
        this.studyTime = studyTime;
        updateSql();
    }

    public void setTestTime(int testTime) {
        this.testTime = testTime;
        updateSql();
    }

    public void addToCheckLists (String title) {
        checklists.put(title, false);
        updateSql();
    }

    public void doneCheckList (String title) {
        checklists.put(title, true);
        updateSql();
    }

    public void undoneCheckList (String title) {
        checklists.put(title, false);
        updateSql();
    }

    public void setNotification(boolean notification) {
        this.notification = notification;
        updateSql();
    }

    public HashMap<String, Boolean> getChecklists () {
        return checklists;
    }

    private void updateSql () {
        AllPlans.updateByID(self_ID, this);
    }
}
